<?php

declare(strict_types = 1);

namespace Drupal\gutenberg_uswds\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Gutenberg USWDS settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'gutenberg_uswds_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['gutenberg_uswds.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['admin_disabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable USWDS libraries in the admin.'),
      '#description' => $this->t('This will disable the USWDS libraries in the admin. This is useful if you want to control the libraries being loaded from an admin theme or by using the gutenberg.yml file'),
      '#default_value' => $this->config('gutenberg_uswds.settings')->get('admin_disabled'),
    ];
    $form['front_disabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable USWDS libraries in the front theme.'),
      '#description' => $this->t('This will disable the USWDS libraries in the front theme. This is useful if you are already using a USWDS based theme.'),
      '#default_value' => $this->config('gutenberg_uswds.settings')->get('front_disabled'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('gutenberg_uswds.settings')
      ->set('admin_disabled', $form_state->getValue('admin_disabled'))
      ->set('front_disabled', $form_state->getValue('front_disabled'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
