/**
 * Import uswds-compile
 */
const { src, dest, parallel, series, watch } = require("gulp");
const autoprefixer = require("autoprefixer");
const sass = require("gulp-sass")(require("sass-embedded"));
const sourcemaps = require("gulp-sourcemaps");
const postcss = require("gulp-postcss");
const uswds = require("@uswds/compile");

/**
 * USWDS version
 * Set the major version of USWDS you're using
 * (Current options are the numbers 2 or 3)
 */
uswds.settings.version = 3;

/**
 * Path settings
 */

let getSrcFrom = (key) => {
  if (uswds.paths.src[key]) {
    return uswds.paths.src[key];
  }
  return uswds.paths.src.defaults[`v${uswds.settings.version}`][key];
};

function buildComponent() {
  const buildSettings = {
    plugins: [
      autoprefixer({
        cascade: false,
        grid: true,
        overrideBrowserslist: uswds.settings.compile.browserslist,
      }),
    ],
    includes: [
      getSrcFrom("uswds"),
      getSrcFrom("sass"),
    ],
  };

  return src('css/sass/*.scss')
    .pipe(sourcemaps.init({ largeFile: true }))
    .pipe(
      sass({ includePaths: buildSettings.includes })
    )
    .pipe(postcss(buildSettings.plugins))
    .pipe(sourcemaps.write("."))
    .pipe(dest('css'));
}


/**
 * Exports
 */
exports.compile = buildComponent;
